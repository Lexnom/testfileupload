from flask import Flask, request, json, send_from_directory
from werkzeug.utils import secure_filename


import shutil
import hashlib
import os
import config

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = config.UPLAOD_FOLDER


# сохранение файла на сервере
def save_file(file):
    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return filename


# получение хэш файла
def hash_file_md5(filename):
    with open(filename, 'rb') as f:
        _hashfile = hashlib.md5()
        while True:
            data = f.read(8192)
            if not data:
                break
            _hashfile.update(data)
    return _hashfile.hexdigest()


# создание директории
def create_dir(path):
    try:
        os.makedirs(path, config.RIGHTS)
        return path
    except OSError:
        return path


# перемещение файла в свою дерикторию, если такая дериктория и файл в нем лежит, удаляем файл
def move_file(old_path, abs_path):
    try:
        shutil.move(old_path, abs_path)
    except OSError:
        os.remove(old_path)

# поиск файла
def search_file(hash, dir):
    for files in os.listdir(dir):
        path = dir + '/' + files
        if os.path.isfile(path):
            return path
        if os.path.isdir(path):
            if files == hash[:2]:
                return search_file(hash, path)
            if files == hash:
                return search_file(hash, path)





@app.route('/api/upload', methods=['POST'])
def upload_file():
    _file = request.files['file']

    filename = save_file(_file)

    old_path_file = app.config['UPLOAD_FOLDER'] + filename

    _hashfile = hash_file_md5(old_path_file)

    absolute_path = create_dir(app.config['UPLOAD_FOLDER'] + _hashfile[:2] + '/' + _hashfile)

    move_file(old_path_file, absolute_path)
    return json.dumps({'hash_file': _hashfile})


@app.route('/api/download', methods=['GET'])
def download_file():

    req = request.get_json()['hash_file']

    abs_path = app.config['UPLOAD_FOLDER']

    file = search_file(req, abs_path)

    return send_from_directory(os.path.dirname(file), filename=os.path.basename(file))

@app.route('/api/delete', methods=['DELETE'])
def delete_file():
    req = request.get_json()['hash_file']
    abs_path = app.config['UPLOAD_FOLDER']
    file = search_file(req, abs_path)
    try:
        os.remove(file)
        return 'success delete'
    except OSError:
        return 'error'


if __name__ == '__main__':
    app.run(host='localhost')
